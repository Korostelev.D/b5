<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
    // Если есть логин в сессии, то пользователь уже авторизован.
    // TODO: Сделать выход (окончание сессии вызовом session_destroy()
    //при нажатии на кнопку Выход).
    if(isset($_POST['button'])){
        session_destroy();

//  TODO: TRY !!!!!

    }
    header('Location: ./');
    // Делаем перенаправление на форму.
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    ?>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <div class="container-my">
        <div class="row">
            <div class="col-md-4">
                <h2> Авторизируйтесь </h2>
                <form action="" method="post">
                    <h5>Логин</h5>
                    <input name="login" />
                    <p></p>
                    <h5>Пароль</h5>
                    <input name="pass" />
                    <p></p>
                    <input type="submit" value="Войти" />
                    <input type="submit" name="button" value="Выйти" />
                </form>
            </div>
        </div>
    </div>


    <?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

    // TODO: Проверть есть ли такой логин и пароль в базе данных.
    // Выдать сообщение об ошибках. ссылка

    $user = 'u24829';
    $pass = '5230726';
    $db = new PDO('mysql:host=localhost;dbname=u24829', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $login_current=$_SESSION['login'];

    $query = $db->query("SELECT * FROM form where login='$login_current'");

    if ($query->num_rows) {
        echo "Query returned 1\n";
    } else {
        echo "Query returned 0\n";
    }

    // Если все ок, то авторизуем пользователя.
    $_SESSION['login'] = $_POST['login'];
    // Записываем ID пользователя.
    $_SESSION['uid'] = 123;




    // Делаем перенаправление.
    header('Location: ./');
}
