<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();

    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('Можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass']));
        }
    }

    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['birthyear'] = !empty($_COOKIE['birthyear_error']);
    $errors['radio1'] = !empty($_COOKIE['radio1_error']);
    $errors['radio2'] = !empty($_COOKIE['radio2_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);
    $errors['super'] = !empty($_COOKIE['super_error']);

    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        $messages[] = '<div class="error">Пусто или ввод недопустимых символов в поле имя!</div>';
    }

    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="error">Пусто или ввод недопустимых символов в поле email!</div>';
    }
    if ($errors['birthyear']) {
        setcookie('birthyear_error', '', 100000);
        $messages[] = '<div class="error">Пусто или неверный ввод данных!</div>';
    }

    if ($errors['radio1']) {
        setcookie('radio1_error', '', 100000);
        $messages[] = '<div class="error">Вы не выбрали пол!</div>';
    }

    if ($errors['radio2']) {
        setcookie('radio2_error', '', 100000);
        $messages[] = '<div class="error">Вы не выбрали кол-во конечностей!</div>';
    }

    if ($errors['bio']) {
        setcookie('bio_error', '', 100000);
        $messages[] = '<div class="error">Вы не заполнили биографию!</div>';
    }

    if ($errors['checkbox']) {
        setcookie('checkbox_error', '', 100000);
        $messages[] = '<div class="error">Вы не отметили согласие на обработку!</div>';
    }
    if ($errors['super']) {
        setcookie('super_error', '', 100000);
        $messages[] = '<div class="error">Вы не выбрали способности!</div>';
    }


    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
    $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
    $values['birthyear'] = empty($_COOKIE['birthyear_value']) ? '' : strip_tags($_COOKIE['birthyear_value']);
    $values['radio1'] = empty($_COOKIE['radio1_value']) ? '' : strip_tags($_COOKIE['radio1_value']);
    $values['radio2'] = empty($_COOKIE['radio2_value']) ? '' : strip_tags($_COOKIE['radio2_value']);
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']);
    $values['checkbox'] = empty($_COOKIE['checkbox_value']) ? '' : strip_tags($_COOKIE['checkbox_value']);
    $values['super'] = empty($_COOKIE['super_value']) ? '' : strip_tags($_COOKIE['super_value']);

    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    if (empty($errors) && !empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
        // TODO: загрузить данные пользователя из БД
        $user = 'u23971';
        $pass = '3457564';
        $db = new PDO('mysql:host=localhost;dbname=u23971', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

        $login_current=$_SESSION['$login'];
        $result = $db->query("SELECT * from form  where login='$login_current'");

        $rows = $result->fetch_all(MYSQLI_ASSOC);
         foreach ($rows as $row) {
        printf("%s (%s)\n", $row["fio"], $row["email"]);
         }
         $values['fio']=$_POST['fio'] ? '' :strip_tags($_SESSION['fio']);
        // и заполнить переменную $values,
        // предварительно санитизовав.
        printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
    }

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
} // Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/",$_POST['fio']) || empty($_POST['fio'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }

// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    } else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 100000);
        // TODO: тут необходимо удалить остальные Cookies.
    }

    // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
        // TODO: перезаписать данные в БД новыми данными,
        // кроме логина и пароля. update
    } else {
        // Генерируем уникальный логин и пароль.
        // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
        $login =substr(md5(uniqid(rand(),true)), 1, 12);
        $pass = substr(md5(uniqid(rand(),true)),2,14);

        print($login);
        print($pass);
        // Сохраняем в Cookies.

        setcookie('login', $login);
        setcookie('pass', $pass);

        // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
        // ...password_hash
        //TODO: do password hash
        $user = 'u23971';
        $pass = '3457564';
        $db = new PDO('mysql:host=localhost;dbname=u23971', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

        $stmt = $db->prepare("INSERT INTO form (name,year,email,sex,limb,bio,checkbox,login,pass) VALUES (:fio,:birthyear,:email, :sex,:limb,:bio,:checkbox,:login,:pass)");
        $stmt -> execute(array('fio'=>$_POST['fio'],'birthyear'=>$_POST['birthyear'],'email'=>$_POST['email'],'sex'=>$_POST['radio1'],'limb'=>$_POST['radio2'],'bio'=>$_POST['bio'],'checkbox'=>$_POST['checkbox'],'login'=>$_POST['login'],'pass'=>$_POST['pass']));

        $form_id =  $db->lastInsertId();
        $myselect = $_POST['super'];
        if (!empty($myselect)) {
            foreach ($myselect as $ability) {
                if (!is_numeric($ability)) {
                    continue;
                }
                $stmt = $db->prepare("INSERT INTO ability (form_id, ability_id) VALUES (:form_id, :ability_id)");
                $stmt -> execute(array(
                    'form_id' => $form_id,
                    'ability_id' => $ability
                ));
            }
        }
    }
    setcookie('save', '1');
    header('Location: ./');
}
